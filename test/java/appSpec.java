import app.Amount;
import app.Main;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigInteger;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Usability unit tests")
public class appSpec {

    @Test
    public void ThousandsNeedsApproval() {
        Main app = new Main();
        Amount a = new Amount(BigInteger.valueOf(1000));
        boolean res = app.needsApproval(a);
        assertTrue(res, () -> "1000 needs approval");
    }

    @Test
    public void FileHundredsDoesNotNeedApproval() {
        Main app = new Main();
        Amount a = new Amount(BigInteger.valueOf(500));
        boolean res = app.needsApproval(a);
        assertFalse(res, () -> "500 does not need approval");
    }

}
