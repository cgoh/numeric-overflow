package app;

import java.math.BigInteger;

public class Amount {
    private BigInteger maxThreshold = BigInteger.valueOf(Integer.MAX_VALUE);
    private BigInteger minThreshold = BigInteger.valueOf(Integer.MIN_VALUE);

    private Integer amount;

    public Amount(BigInteger amount){
        if(!isValid(amount)) {
            throw new IllegalArgumentException("Amount needs approval");
        }
        this.amount = amount.intValue();
    }

    public boolean isLesserThan(Amount otherAmount){
        return compare(this,otherAmount) == -1;
    }

    private boolean isValid(BigInteger amount) {
        return (amount.compareTo(minThreshold) + amount.compareTo(maxThreshold)) == 0;
    }

    private int compare(Amount a, Amount b)
    {
        return a.amount.compareTo(b.amount);
    }

}
