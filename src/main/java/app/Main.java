package app;

import java.math.BigInteger;

public class Main {

    private Amount threshold = new Amount(BigInteger.valueOf((1000)));

    public static void main(String[] args) {

        Main app = new Main();

        Amount amount = new Amount(BigInteger.valueOf((999)));
        if (!app.needsApproval(amount)) {
            System.out.println(amount + " does not require approval");
        }
        Amount amount2 = new Amount(BigInteger.valueOf(2000));
        if (app.needsApproval(amount2)) {
            System.out.println(amount + " requires approval");
        }
    }

    public boolean needsApproval(Amount amount) {
        if (amount.isLesserThan(threshold)) {
            return false;
        }
        return true;
    }

}
